<?php
declare(strict_types=1);

namespace App\Tests;

use App\Models\Invest;
use App\Models\Loan;
use App\Models\Tranche;
use App\Models\Investor;
use App\Exceptions\InvestLimitReachedException;
use App\Factories\InvestFactory;
use App\Factories\InvestorFactory;
use App\Factories\LoanFactory;
use App\Factories\TrancheFactory;
use App\Managers\InvestManager;
use App\Services\CalculatorService;
use DateTime;
use PHPUnit\Framework\TestCase;

class CalculatorServiceTest extends TestCase
{
    /**
     * @var int
     */
    protected $amountLimit;

    /**
     * @var CalculatorService
     */
    protected $calculationService;

    /**
     * @var TrancheFactory
     */
    protected $trancheFactory;

    /**
     * @var InvestFactory
     */
    protected $investFactory;

    /**
     * @var LoanFactory
     */
    protected $loanFactory;


    public function setUp(): void
    {
        $this->amountLimit = 1000;
        $this->calculationService = new CalculatorService();
        $this->trancheFactory = new TrancheFactory();
        $this->investFactory = new InvestFactory();
        $this->loanFactory = new LoanFactory();
    }

    public function testWithoutInvests(): void
    {
        $loan = $this->loanFactory->create(5555, new DateTime('1/10/2015'), new DateTime('15-11-2015'));
        $investor = new Investor('Investor 1', $this->amountLimit);

        $this->calculationService->setLoan($loan);
        $this->calculationService->calculate(new DateTime('01-10-2015'), new DateTime('31-10-2015'));
        $result = $this->calculationService->getInterestForInvestor($investor);

        $this->assertNull($result);
    }

    public function testOneInvest(): void
    {
        $loan = $this->loanFactory->create(5555, new DateTime('1/10/2015'), new DateTime('15-11-2015'));
        $tranche = $this->trancheFactory->create($loan, 3, $this->amountLimit, 'some name');
        $investor = new Investor('Investor 1', $this->amountLimit);

        $invest = $this->investFactory->create($tranche, $investor, $this->amountLimit, new DateTime('03-10-2015'));

        $this->calculationService->setLoan($loan);
        $result = $this->calculationService->calculateInterest($invest);

        $this->assertEqualsWithDelta(28.06, $result, 0.01);
    }

    public function testManyInvests(): void
    {
        $loan = $this->loanFactory->create(5555, new DateTime('01-10-2015'), new DateTime('15-11-2015'));
        $tranche = $this->trancheFactory->create($loan, 3, $this->amountLimit, 'some name');
        $investor = new Investor('Investor 1', $this->amountLimit);

        $invest1 = $this->investFactory->create($tranche, $investor, 100, new DateTime('03-10-2015'));
        $invest2 = $this->investFactory->create($tranche, $investor, 300, new DateTime('05-10-2015'));
        $invest3 = $this->investFactory->create($tranche, $investor, 500, new DateTime('09-10-2015'));

        $this->calculationService->setLoan($loan);
        $result = $this->calculationService->calculateInterest($invest1)
            + $this->calculationService->calculateInterest($invest2)
            + $this->calculationService->calculateInterest($invest3)
        ;

        $this->calculationService->calculate(new DateTime('01-10-2015'), new DateTime('31-10-2015'));
        $result2 = $this->calculationService->getInterestForInvestor($investor);

        $this->assertEqualsWithDelta($result, $result2, 0.01);
    }

}

