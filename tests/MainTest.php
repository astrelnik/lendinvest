<?php
declare(strict_types=1);

namespace App\Tests;

use App\Models\Invest;
use App\Exceptions\InvestLimitReachedException;
use App\Factories\InvestFactory;
use App\Factories\InvestorFactory;
use App\Factories\LoanFactory;
use App\Factories\TrancheFactory;
use App\Managers\InvestManager;
use App\Services\CalculatorService;
use DateTime;
use PHPUnit\Framework\TestCase;

class MainTest extends TestCase
{
    protected $amountLimit;
    protected $investFactory;
    protected $investorFactory;
    protected $trancheFactory;
    protected $loanFactory;
    protected $investManager;
    protected $calculationService;

    public function setUp(): void
    {
        $this->amountLimit = 1000;
        $this->investFactory = new InvestFactory();
        $this->investorFactory = new InvestorFactory();
        $this->trancheFactory = new TrancheFactory();
        $this->loanFactory = new LoanFactory();
        $this->investManager = new InvestManager($this->investFactory);
        $this->calculationService = new CalculatorService();
    }

    public function testFirstInvestToTrancheA(): void
    {
        $loan = $this->loanFactory->create(4555, new DateTime('1/10/2015'), new DateTime('15-11-2015'));
        $trancheA = $this->trancheFactory->create($loan, 3, $this->amountLimit, 'A');
        $investor1 = $this->investorFactory->create('Investor 1');
        $invest = $this->investManager->investToTranche($trancheA, $investor1, $this->amountLimit, new DateTime('03-10-2015'));

        $this->assertInstanceOf(Invest::class, $invest);
    }

    public function testSecondInvestToTrancheA(): void
    {
        $loan = $this->loanFactory->create(4555, new DateTime('1/10/2015'), new DateTime('15-11-2015'));
        $trancheA = $this->trancheFactory->create($loan, 3, $this->amountLimit, 'A');
        $investor1 = $this->investorFactory->create('Investor 1');
        $investor2 = $this->investorFactory->create('Investor 2');

        $this->investManager->investToTranche($trancheA, $investor1, $this->amountLimit, new DateTime('03-10-2015'));

        $this->expectException(InvestLimitReachedException::class);
        $this->investManager->investToTranche($trancheA, $investor2, 1, new DateTime('04-10-2015'));
    }

    public function testFirstInvestToTrancheB(): void
    {
        $loan = $this->loanFactory->create(5555, new DateTime('1/10/2015'), new DateTime('15-11-2015'));
        $trancheB = $this->trancheFactory->create($loan, 6, $this->amountLimit, 'B');
        $investor3 = $this->investorFactory->create('Investor 3');

        $invest = $this->investManager->investToTranche($trancheB, $investor3, 500, new DateTime('10-10-2015'));
        $this->assertInstanceOf(Invest::class, $invest);
    }

    public function testSecondInvestToTrancheB(): void
    {
        $loan = $this->loanFactory->create(5555, new DateTime('1/10/2015'), new DateTime('15-11-2015'));
        $trancheB = $this->trancheFactory->create($loan, 6, $this->amountLimit, 'B');
        $investor3 = $this->investorFactory->create('Investor 3');
        $investor4 = $this->investorFactory->create('Investor 4');

        $this->investManager->investToTranche($trancheB, $investor3, 500, new DateTime('10-10-2015'));
        $this->expectException(InvestLimitReachedException::class);
        $this->investManager->investToTranche($trancheB, $investor4, 1100, new DateTime('25-10-2015'));
    }

    public function testInterests(): void
    {
        $loan = $this->loanFactory->create(5555, new DateTime('1/10/2015'), new DateTime('15-11-2015'));
        $trancheA = $this->trancheFactory->create($loan, 3, $this->amountLimit, 'A');
        $trancheB = $this->trancheFactory->create($loan, 6, $this->amountLimit, 'B');
        $investor1 = $this->investorFactory->create('Investor 1');
        $investor3 = $this->investorFactory->create('Investor 3');

        $this->investManager->investToTranche($trancheA, $investor1, $this->amountLimit, new DateTime('03-10-2015'));
        $this->investManager->investToTranche($trancheB, $investor3, 500, new DateTime('10-10-2015'));

        $this->calculationService->setLoan($loan);
        $this->calculationService->calculate(new DateTime('01-10-2015'), new DateTime('31-10-2015'));

        $this->assertEqualsWithDelta(28.06, $this->calculationService->getInterestForInvestor($investor1), 0.01);
        $this->assertEqualsWithDelta(21.29, $this->calculationService->getInterestForInvestor($investor3), 0.01);
    }

}

