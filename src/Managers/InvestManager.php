<?php
declare(strict_types=1);

namespace App\Managers;

use App\Models\Invest;
use App\Models\Investor;
use App\Models\Tranche;
use App\Exceptions\InvestLimitReachedException;
use App\Factories\InvestFactory;
use DateTime;

class InvestManager
{
    /**
     * @var InvestFactory
     */
    private $investFactory;

    /**
     * InvestManager constructor.
     * @param InvestFactory $investFactory
     */
    public function __construct(InvestFactory $investFactory)
    {
        $this->investFactory = $investFactory;
    }

    /**
     * @param Tranche $tranche
     * @param Investor $investor
     * @param float $amount
     * @param DateTime|null $date
     * @return Invest
     */
    public function investToTranche(Tranche $tranche, Investor $investor, float $amount, DateTime $date = null): Invest
    {
        if ($tranche->getMaxAmount() < $amount + $tranche->getDonated()) {
            throw new InvestLimitReachedException();
        }

        $amount = $investor->take($amount);

        return $this->investFactory->create($tranche, $investor, $amount, $date);
    }
}
