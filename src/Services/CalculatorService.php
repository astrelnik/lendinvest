<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\Invest;
use App\Models\Investor;
use App\Models\Loan;
use DateTime;

class CalculatorService
{
    /**
     * @var float[]
     */
    private $results;

    /**
     * @var Loan
     */
    protected $loan;

    /**
     * @param Loan $loan
     */
    public function setLoan(Loan $loan): void
    {
        $this->loan = $loan;
    }

    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     */
    public function calculate(DateTime $dateFrom, DateTime $dateTo): void
    {
        foreach ($this->loan->getTranches() as $tranche) {
            foreach ($tranche->getInvests() as $invest) {
                $investDate = $invest->getDate();
                if ($investDate >= $dateFrom && $investDate <= $dateTo) {
                    $earnings = $this->calculateInterest($invest);
                    if (isset($this->results[$invest->getInvestor()->getIdentifier()])) {
                        $this->results[$invest->getInvestor()->getIdentifier()] += $earnings;
                    } else {
                        $this->results[$invest->getInvestor()->getIdentifier()] = $earnings;
                    }
                }
            }
        }
    }

    /**
     * @param Invest $invest
     * @return float
     */
    public function calculateInterest(Invest $invest): float
    {
        $daysInMonth = (int) $invest->getDate()->format('t');
        $profitableDays = (int) $daysInMonth - $invest->getDate()->format('j') + 1;

        return $invest->getAmount() / $daysInMonth *
            $invest->getTranche()->getMonthlyInterestRate() / 100 * $profitableDays;
    }

    /**
     * @param Investor $investor
     * @return float
     */
    public function getInterestForInvestor(Investor $investor): ?float
    {
        return $this->results[$investor->getIdentifier()] ?? null;
    }
}