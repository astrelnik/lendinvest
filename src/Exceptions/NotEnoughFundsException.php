<?php
declare(strict_types=1);

namespace App\Exceptions;

use LogicException;

class NotEnoughFundsException extends LogicException
{
    /**
     * NotEnoughFundsException constructor.
     * @param string $message
     */
    public function __construct(string $message = 'Not Enough Funds')
    {
        parent::__construct($message, 500);
    }
}
