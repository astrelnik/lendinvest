<?php
declare(strict_types=1);

namespace App\Exceptions;

use LogicException;

class IsInvestExpiredLoanException extends LogicException
{
    public function __construct()
    {
        parent::__construct('Is invest try to expired loan', 500);
    }
}