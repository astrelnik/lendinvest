<?php
declare(strict_types=1);

namespace App\Exceptions;

use LogicException;

class InvestLimitReachedException extends LogicException
{
    /**
     * InvestLimitReachedException constructor.
     * @param string $message
     */
    public function __construct($message = 'Invest Limit Reached')
    {
        parent::__construct($message, 500);
    }
}