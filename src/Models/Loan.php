<?php
declare(strict_types=1);

namespace App\Models;

use DateTime;

class Loan
{
    /**
     * @var DateTime
     */
    protected $dateStart;

    /**
     * @var DateTime
     */
    protected $dateEnd;

    /**
     * @var float
     */
    protected $amount;

    /**
     * @var Tranche[]
     */
    protected $tranches = [];

    /**
     * Loan constructor.
     * @param float $amount
     * @param DateTime $start
     * @param DateTime $end
     */
    public function __construct(float $amount, DateTime $start, DateTime $end)
    {
        $this->amount = $amount;
        $this->dateStart = $start;
        $this->dateEnd = $end;
    }

    /**
     * @return DateTime
     */
    public function getDateStart(): DateTime
    {
        return $this->dateStart;
    }

    /**
     * @return DateTime
     */
    public function getDateEnd(): DateTime
    {
        return $this->dateEnd;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param Tranche $tranche
     */
    public function addTranche(Tranche $tranche): void
    {
        $this->tranches[] = $tranche;
    }

    /**
     * @return Tranche[]
     */
    public function getTranches(): array
    {
        return $this->tranches;
    }
}
