<?php
declare(strict_types=1);

namespace App\Models;

use App\Exceptions\NotEnoughFundsException;

class Investor
{
    /**
     * @var float
     */
    protected $balance = 1000;

    /**
     * @var string
     */
    protected $name;

    /**
     * Investor constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param float $amount
     */
    public function addMoney(float $amount): void
    {
        $this->balance += $amount;
    }

    /**
     * @return mixed|string
     */
    public function getIdentifier()
    {
        return $this->getName();
    }

    /**
     * @param float $amount
     * @return float
     */
    public function take(float $amount): float
    {
        if ($this->balance < $amount) {
            throw new NotEnoughFundsException();
        }

        $this->balance -= $amount;

        return $amount;
    }
}
