<?php
declare(strict_types=1);

namespace App\Models;

use App\Exceptions\InvestLimitReachedException;

class Tranche
{
    /**
     * @var float
     */
    private $donated = 0;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Loan
     */
    protected $loan;

    /**
     * @var float
     */
    protected $monthlyInterestRate;

    /**
     * @var float
     */
    protected $maxAmount;

    /**
     * @var Invest[]
     */
    protected $invests = [];

    /**
     * Tranche constructor.
     * @param Loan $loan
     * @param float $monthlyInterestRate
     * @param float $maxAmount
     * @param string $name
     */
    public function __construct(Loan $loan, float $monthlyInterestRate, float $maxAmount, string $name)
    {
        $this->loan = $loan;
        $this->monthlyInterestRate = $monthlyInterestRate;
        $this->maxAmount = $maxAmount;
        $this->name = $name;
    }

    /**
     * @return Loan
     */
    public function getLoan(): Loan
    {
        return $this->loan;
    }

    /**
     * @return float
     */
    public function getMonthlyInterestRate(): float
    {
        return $this->monthlyInterestRate;
    }

    /**
     * @return float
     */
    public function getMaxAmount(): float
    {
        return $this->maxAmount;
    }

    /**
     * @param Invest $invest
     */
    public function addInvest(Invest $invest): void
    {
        if ($this->maxAmount < $this->donated + $invest->getAmount() ) {
            throw new InvestLimitReachedException();
        }

        $this->invests[] = $invest;
        $this->donated += $invest->getAmount();
    }

    /**
     * @return float
     */
    public function getDonated(): float
    {
        return $this->donated;
    }

    /**
     * @return Invest[]
     */
    public function getInvests(): array
    {
        return $this->invests;
    }
}
