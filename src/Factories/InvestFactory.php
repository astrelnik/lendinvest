<?php
declare(strict_types=1);

namespace App\Factories;

use App\Models\Invest;
use App\Models\Investor;
use App\Models\Tranche;
use App\Exceptions\IsInvestExpiredLoanException;
use DateTime;

class InvestFactory
{
    /**
     * @param Tranche $tranche
     * @param Investor $investor
     * @param float $amount
     * @param DateTime|null $date
     * @return Invest
     * @throws
     */
    public function create(Tranche $tranche, Investor $investor, float $amount, DateTime $date = null): Invest
    {
        $date = $date ?? new DateTime();

        if ($date < $tranche->getLoan()->getDateStart() || $date > $tranche->getLoan()->getDateEnd()) {
            throw new IsInvestExpiredLoanException();
        }

        $invest = new Invest($tranche, $investor, $amount, $date);
        $tranche->addInvest($invest);
        return $invest;
    }
}